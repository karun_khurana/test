const Sequelize = require('sequelize');

var Data;
function initiateUser(){
	Data = sequelize.define('data', {
	  name: {
	    type: Sequelize.STRING
	  }
	});
	sequelize.sync();

}

const sequelize = new Sequelize('testPackage', 'root', '12345', {
  host: 'localhost',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },

});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
    initiateUser()
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });



module.exports.api = {
  add: function(env, http, args) {
  	Data.create({
	    name: args.item
	});
    return Promise.resolve('Submitted successfully');
  },

  list: function(env, http, args) {
  	return Data.findAll(); 
  }

};
